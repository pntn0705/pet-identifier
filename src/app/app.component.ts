import { Component, OnInit } from '@angular/core';
import {
  DynamicModel,
  IDynamicFormField,
} from './modules/dynamic-form/interfaces/dynamic-form-field.interface';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    const observable = new Observable((subscriber) => {
      subscriber.next(1);
      subscriber.next(2);
      subscriber.next(3);
      setTimeout(() => {
        subscriber.next(4);
        subscriber.complete();
      }, 2000);
    });

    console.log('just before subscribe');
    let value = '';
    observable.subscribe({
      next(x: any) {
        value = x;
      },
      error(err) {
        console.error('something wrong occurred: ' + err);
      },
      complete() {
        console.log('final value is:', value);
      },
    });
  }

  title = 'pet-identifier';

  model: DynamicModel = {
    name: {
      label: 'Your name',
      type: 'text',
    },
    subscriptionType: {
      label: 'Subscription Type',
      defaultValue: 'premium',
      type: 'select',
      options: [
        {
          label: 'Pick one',
          value: '',
        },
        {
          label: 'Premium',
          value: 'premium',
        },
        {
          label: 'Basic',
          value: 'basic',
        },
      ],
    },
  };
}
