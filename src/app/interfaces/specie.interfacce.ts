export interface ISpecie {
  id: number;
  specie_code: number;
  name: string;
}
