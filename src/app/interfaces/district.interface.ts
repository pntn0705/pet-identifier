export interface IDistrict {
  district_id: number;
  name: string;
}
