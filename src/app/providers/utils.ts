import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function birthDateValidate(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value: Date = control.value;
    if (!value) {
      return null;
    }
    value.setHours(0, 0, 0, 0);
    const date = new Date();
    if (value.getTime() > date.getTime()) {
      return { validBirth: true };
    }

    return null;
  };
}

export function makeCacheKey(objectName: string, id: string | number): string {
  return `${objectName}_${id}`;
}
