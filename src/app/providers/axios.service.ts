import {
  HttpClient,
  HttpContext,
  HttpHeaders,
  HttpParams,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { ICountry } from '../interfaces/country.interface';
import { ISpecie } from '../interfaces/specie.interfacce';
import { ITribe } from '../interfaces/tribe.interface';
import { IProvince } from '../interfaces/province.interface';
import { IDistrict } from '../interfaces/district.interface';
import { ICommune } from '../interfaces/commune.interface';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { makeCacheKey } from './utils';

@Injectable({ providedIn: 'root' })
export class AxiosService {
  constructor(
    public httpClient: HttpClient,
    private dbService: NgxIndexedDBService
  ) {}

  getCache(key: string) {
    return new Promise((resolve) => {
      const $ = this.dbService.getByKey('cache', key);

      $.subscribe({
        error(err) {
          console.error(err);
        },

        complete() {},
        next(val: any) {
          console.log(val);
          if (val && val?.data.length > 0) {
            if (new Date(val.expiredAt) > new Date()) {
              return resolve(val.data);
            }
          }

          resolve([]);
        },
      });
    });
  }

  async makeGetRequest(key: string, url: string) {
    const $ = this.httpClient.get(url);

    const save = (key: string, data: any[]) => {
      const expiredAt = new Date();
      expiredAt.setMinutes(expiredAt.getMinutes() + 2);

      this.dbService
        .add('cache', {
          data,
          expiredAt,
          id: key,
        })
        .subscribe((key) => console.log('key', key));
    };

    return new Promise((resolve) => {
      this.dbService.deleteByKey('cache', key).subscribe((val) => {
        console.log('call api', val, 'cleared');
        $.subscribe({
          next(value: any) {
            console.log('receive data');
            const data = value?.data;
            save(key, data);
            resolve(data);
          },
        });
      });
    });
  }

  async getCountries(): Promise<ICountry[]> {
    return this.getV2<ICountry>(
      'country',
      'https://api.cancuocthucung.vn/v1/countries'
    );
  }

  async getSpecies(): Promise<ISpecie[]> {
    return this.getV2<ISpecie>(
      'specie',
      'https://api.cancuocthucung.vn/v1/species'
    );
  }

  async getV2<T>(key: string, url: string): Promise<T[]> {
    const tValue = (await this.getCache(key)) as T[];

    if (tValue.length > 0) {
      console.log('get data from db');
      return tValue;
    }

    console.log('get data from API');
    return this.makeGetRequest(key, url) as Promise<T[]>;
  }

  async getTribes(code: number): Promise<ITribe[]> {
    return this.getV2<ITribe>(
      makeCacheKey('tribe', code.toString()),
      `https://api.cancuocthucung.vn/v1/tribes?specie_code=${code}`
    );
  }

  async getProvinces(): Promise<IProvince[]> {
    return this.getV2<IProvince>(
      'province',
      'https://api.cancuocthucung.vn/v1/provinces'
    );
  }

  async getDistricts(code: number): Promise<IDistrict[]> {
    return this.getV2<IDistrict>(
      makeCacheKey('district', code.toString()),
      `https://api.cancuocthucung.vn/v1/districts?province_id=${code}`
    );
  }

  async getCommunes(code: number): Promise<ICommune[]> {
    return this.getV2<ICommune>(
      makeCacheKey('commune', code),
      `https://api.cancuocthucung.vn/v1/communes?district_id=${code}`
    );
  }
}
