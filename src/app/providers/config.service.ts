import { DBConfig, ObjectStoreMeta } from 'ngx-indexed-db';

export class ConfigService {
  static getDbConfig(): DBConfig {
    return {
      name: 'petIdentifier',
      version: 1,
      objectStoresMeta: ConfigService.getObjectStoresMeta(),
    };
  }

  static getObjectStoresMeta(): ObjectStoreMeta[] {
    return [
      {
        store: 'cache',
        storeConfig: { keyPath: 'id', autoIncrement: false },
        storeSchema: [
          { name: 'data', keypath: 'data', options: { unique: false } },
          { name: 'id', keypath: 'id', options: { unique: true } },
          {
            name: 'expiredAt',
            keypath: 'expiredAt',
            options: { unique: false },
          },
        ],
      },
    ];
  }
}
