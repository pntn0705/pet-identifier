import { NgModule, isDevMode } from '@angular/core';
import {
  BrowserModule,
  provideClientHydration,
} from '@angular/platform-browser';

import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import en from '@angular/common/locales/en';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { getDatabase, provideDatabase } from '@angular/fire/database';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';
import { DBConfig, NgxIndexedDBModule } from 'ngx-indexed-db';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatComponent } from './modules/chat/chat.component';
import { DynamicModule } from './modules/dynamic-form/dynamic-form.module';
import { MeetingComponent } from './modules/meeting/meeting.component';
import { RegisterModule } from './modules/register/register.module';
import { ConfigService } from './providers/config.service';
import { AngularFireModule } from '@angular/fire/compat';

registerLocaleData(en);

const dbConfig: DBConfig = ConfigService.getDbConfig();

const config = {
  apiKey: 'AIzaSyCke_PKRYxrZARHjEI-iZNR0z3I3JoJBK0',
  databaseURL:
    'https://pet-identifier-1f67f-default-rtdb.asia-southeast1.firebasedatabase.app',
  authDomain: 'pet-identifier-1f67f.firebaseapp.com',
  projectId: 'pet-identifier-1f67f',
  storageBucket: 'pet-identifier-1f67f.appspot.com',
  messagingSenderId: '7355391527',
  appId: '1:7355391527:web:390b23926c53952a4ab802',
};

@NgModule({
  declarations: [AppComponent, ChatComponent],
  imports: [
    BrowserModule,
    provideFirebaseApp(() => initializeApp(config)),
    provideDatabase(() => getDatabase()),

    // provideFirebaseApp(() => initializeApp(config)),
    // provideFirestore(() => getFirestore()),
    AngularFireModule.initializeApp(config),
    AppRoutingModule,
    RegisterModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DynamicModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: !isDevMode(),
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000',
    }),
    NgxIndexedDBModule.forRoot(dbConfig),
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }, provideClientHydration()],
  bootstrap: [AppComponent],
})
export class AppModule {}
