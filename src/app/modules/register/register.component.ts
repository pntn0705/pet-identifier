import { Component, Input, OnInit, Output } from '@angular/core';
import { IRegister } from './register.interface';
import { NgxIndexedDBService } from 'ngx-indexed-db';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: [
    './register.component.scss',
    './register-responsive.component.scss',
  ],
})
export class RegisterComponent implements OnInit {
  constructor(private dbService: NgxIndexedDBService) {}

  props = {
    country: { country_id: 0, name: '' },
    specie: { specie_code: 0, id: 0, name: '' },
    tribe: { name: '' },
    gender: null,
    birthDate: null,
    hometown: { province_id: 0, name: '' },
    province: { province_id: 0, name: '' },
    origin: { country_id: 0, name: '' },
    district: { district_id: 0, name: '' },
    commune: { commune_id: 0, name: '' },
    name: '',
    address: '',
    cropImgPreview: '',
  };

  @Input()
  count: number = 1;
  @Input()
  totalAmount: number = 150000;

  canSubmit = false;

  onValidFormChange(value: boolean) {
    this.canSubmit = value;
  }

  onCountChange(event: any) {
    this.count = event;
    this.totalAmount = 150000 * this.count;
  }

  onPropsChange(event: any) {
    this.props = { ...this.props, ...event };
  }

  frontActive = true;

  frontActiveChange(value: boolean) {
    this.frontActive = value;
  }

  ngOnInit(): void {}

  get birthDateString() {
    if (!this.props.birthDate) {
      return null;
    }
    const date = new Date(this.props.birthDate);

    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const d = date.getDate();

    return `${d}/${month}/${year}`;
  }

  get addressString(): string {
    let address = this.props.address ? this.props.address + ', ' : '';
    address += this.props.commune.name ? this.props.commune.name + ', ' : '';
    address += this.props.district.name ? this.props.district.name + ', ' : '';
    address += this.props.province.name || '';

    return address;
  }
}
