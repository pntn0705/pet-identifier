import { ICommune } from 'src/app/interfaces/commune.interface';
import { ICountry } from 'src/app/interfaces/country.interface';
import { IDistrict } from 'src/app/interfaces/district.interface';
import { IProvince } from 'src/app/interfaces/province.interface';
import { ISpecie } from 'src/app/interfaces/specie.interfacce';
import { ITribe } from 'src/app/interfaces/tribe.interface';

export interface IRegister {
  country: ICountry;
  specie: ISpecie;
  tribe: ITribe;
  gender: 'Đực' | 'Cái';
  birthDate: Date;
  origin: ICountry;
  hometown: IProvince;
  province: IProvince;
  district: IDistrict;
  commune: ICommune;
  name: string;
  address: string;
  cropImgPreview: any;
}
