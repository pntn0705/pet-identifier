import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-payment-card',
  templateUrl: './payment-card.component.html',
  styleUrls: ['./payment-card.component.scss'],
})
export class PaymentCardComponent {
  constructor() {}

  @Input()
  count: number = 1;

  @Input()
  totalAmount: number = 150000;

  @Input()
  canSubmit: boolean = false;
}
