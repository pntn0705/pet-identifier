import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';

import { FormGroup, Validators } from '@angular/forms';
import { ICommune } from 'src/app/interfaces/commune.interface';
import { ICountry } from 'src/app/interfaces/country.interface';
import { IDistrict } from 'src/app/interfaces/district.interface';
import { IProvince } from 'src/app/interfaces/province.interface';
import { ISpecie } from 'src/app/interfaces/specie.interfacce';
import { ITribe } from 'src/app/interfaces/tribe.interface';
import { DynamicModel } from 'src/app/modules/dynamic-form/interfaces/dynamic-form-field.interface';
import { AxiosService } from 'src/app/providers/axios.service';
import { birthDateValidate } from 'src/app/providers/utils';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: [
    './register-form.component.scss',
    'register-form-responsive.scss',
  ],
})
export class RegisterFormComponent implements OnInit, AfterViewInit {
  constructor(private readonly axiosService: AxiosService) {}

  @Output()
  onPropsChange = new EventEmitter();

  async onFormChange(event: { name: string; value: any }) {
    switch (event.name) {
      case 'specie':
        await this.onSpecieChange(event.value);
        event.value = this.props.specie;
        break;
      case 'tribe':
        this.onTribeChange(event.value);
        event.value = this.props.tribe;
        break;
      case 'origin':
        this.onOriginChange(event.value);
        event.value = this.props.origin;
        break;
      case 'hometown':
        this.onHomeTownChange(event.value);
        event.value = this.props.hometown;
        break;
      case 'province':
        await this.onProvinceChange(event.value);
        event.value = this.props.province;
        break;
      case 'district':
        await this.onDistrictChange(event.value);
        event.value = this.props.district;
        break;
      case 'commune':
        this.onCommuneChange(event.value);
        event.value = this.props.commune;
        break;
    }
    this.props[event.name] = event.value;
    this.onPropsChange.emit({ [event.name]: event.value });
  }

  countries: ICountry[] = [];
  species: ISpecie[] = [];
  tribes: ITribe[] = [];
  genders: string[] = ['Đực', 'Cái'];
  provinces: IProvince[] = [];
  hometowns: IProvince[] = [];
  districts: IDistrict[] = [];
  communes: ICommune[] = [];

  model: DynamicModel = {
    province: {
      label: 'Tỉnh / Thành phố',
      type: 'select',
      options: [],
      validators: [Validators.required],
    },
    district: {
      label: 'Quận / Huyện',
      type: 'select',
      options: [],
    },
    commune: {
      label: 'Phường / Xã',
      type: 'select',
      options: [],
    },
    address: {
      label: 'Đường / Phố',
      type: 'text',
      validators: [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(100),
        Validators.nullValidator,
      ],
      errorMessage:
        'Địa chỉ có độ dài ít nhất là 10, và không dài hơn 100 ký tự',
    },
  };

  nameModel: DynamicModel = {
    name: {
      label: 'Tên Boss',
      type: 'text',
      validators: [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
        Validators.nullValidator,
      ],
      errorMessage: 'Tên có độ dài lớn hơn 1, nhỏ hơn 50',
    },
  };

  specieModel: DynamicModel = {
    specie: {
      label: 'Loài',
      type: 'select',
      options: [],
    },
    tribe: {
      label: 'Bộ tộc',
      type: 'select',
      options: [],
    },
  };

  originModel: DynamicModel = {
    birthDate: {
      label: 'Ngày sinh',
      type: 'date',
      validators: [Validators.required, birthDateValidate()],
      errorMessage: 'Ngày sinh phải nhỏ hơn hoặc bằng hiện tại',
    },
    gender: {
      label: 'Giới tính',
      type: 'select',
      options: [
        { value: 'Đực', label: 'Đực' },
        { value: 'Cái', label: 'Cái' },
      ],
    },
    origin: {
      label: 'Nguồn gốc',
      type: 'select',
      options: [],
    },
    hometown: {
      label: 'Quê quán',
      type: 'select',
      options: [],
    },
  };

  validImg = false;

  isValidForm(
    event: boolean,
    modelName?: 'name-info' | 'specie-info' | 'origin-info' | 'address-info'
  ) {
    switch (modelName) {
      case 'name-info':
        this.nameModel.isValid = event;
        break;
      case 'specie-info':
        this.specieModel.isValid = event;
        break;
      case 'origin-info':
        this.originModel.isValid = event;
        break;
      case 'address-info':
        this.model.isValid = event;
    }
    if (
      !this.nameModel.isValid ||
      !this.specieModel.isValid ||
      !this.originModel.isValid ||
      !this.model.isValid ||
      !this.validImg
    ) {
      console.log('form invalid');
      this.isValid.emit(false);
      return;
    }

    console.log('form valid');
    this.isValid.emit(true);
  }

  @ViewChild('uploadInput')
  uploadInput: any;

  ngAfterViewInit(): void {}

  zoomChange(isIncrement = true) {
    if (isIncrement) {
      if (this.zoom > 2.9) {
        return;
      }

      this.zoom += 0.1;
      return;
    }

    if (this.zoom < 1.1) {
      return;
    }

    this.zoom -= 0.1;
  }

  @Input()
  count = 0;
  @Input()
  totalAmount = 0;
  @Output()
  onCountChange = new EventEmitter<number>();
  @Output()
  isValid = new EventEmitter<boolean>();

  zoom = 1.0;
  rotate = 0;

  amountChange() {
    this.totalAmount = 150000 * this.count;
  }

  countChange(isIncrement = true) {
    if (isIncrement) {
      this.count++;
      this.onCountChange.emit(this.count);
      this.amountChange();
      return;
    }

    if (this.count <= 1) {
      return;
    }

    this.count--;

    this.onCountChange.emit(this.count);
    this.amountChange();
  }

  isCopperVisible = false;

  props: any = {
    country: { country_id: 0, name: '' },
    specie: { specie_code: 0, id: 0, name: '' },
    tribe: { name: '' },
    gender: null,
    birthDate: null,
    hometown: { province_id: 0, name: '' },
    province: { province_id: 0, name: '' },
    origin: { country_id: 0, name: '' },
    district: { district_id: 0, name: '' },
    commune: { commune_id: 0, name: '' },
    name: '',
    address: '',
    cropImgPreview: '',
  };
  registerForm!: FormGroup;
  disableTribes = true;

  async ngOnInit(): Promise<void> {
    this.countries = await this.axiosService.getCountries();
    this.species = await this.axiosService.getSpecies();
    this.specieModel['specie'].options = this.species.map((s) => ({
      value: s.id,
      label: s.name,
    }));
    this.provinces = await this.axiosService.getProvinces();
    this.model['province'].options = this.provinces.map((p) => ({
      value: p.province_id,
      label: p.name,
    }));

    this.originModel['origin'].options = this.countries.map((c) => ({
      label: c.name,
      value: c.country_id,
    }));

    this.originModel['hometown'].options = [...this.model['province'].options];
    this.hometowns = this.provinces;
    this.totalAmount = 150000 * this.count;
  }

  onOriginChange(event: any | string) {
    const value = typeof event === 'string' ? +event : +event.target.value;
    const countryObj = this.countries.find((s) => s.country_id === value);
    if (countryObj) {
      this.props.origin = countryObj;
    }
  }

  async onSpecieChange(event: any | 'string') {
    const value = typeof event === 'string' ? +event : +event.target.value;
    const specieObj = this.species.find((s) => s.specie_code === value);
    if (specieObj) {
      this.props.specie = specieObj;
      this.tribes = await this.axiosService.getTribes(this.props.specie.id);
      this.disableTribes = false;
      this.specieModel['tribe'].options = this.tribes.map((t) => ({
        value: t.name,
        label: t.name,
      }));
    }
  }
  onTribeChange(event: any | string) {
    this.props.tribe =
      typeof event === 'string'
        ? { name: event }
        : { name: event.target.value };
  }

  onBirthChange(event: any) {}

  onGenderChange(event: any) {
    this.props.gender = event.target.value;
  }

  async onProvinceChange(event: any | 'string') {
    const value = typeof event === 'string' ? +event : +event.target.value;
    const provinceObj = this.hometowns.find((s) => s.province_id === value);

    if (provinceObj) {
      this.props.province = provinceObj;
      this.onPropsChange.next({ province: this.props.province });
      this.districts = await this.axiosService.getDistricts(
        this.props.province.province_id
      );

      this.model['district'].options = this.districts.map((p) => ({
        value: p.district_id,
        label: p.name,
      }));
    }
  }

  onHomeTownChange(event: any | 'string') {
    const value = typeof event === 'string' ? +event : +event.target.value;
    const provinceObj = this.hometowns.find((s) => s.province_id === value);
    if (provinceObj) {
      this.props.hometown = provinceObj;
    }
  }

  async onDistrictChange(event: any | string) {
    const value = typeof event === 'string' ? +event : +event.target.value;
    const districtObj = this.districts.find((s) => s.district_id === value);
    if (districtObj) {
      this.props.district = districtObj;
      this.communes = await this.axiosService.getCommunes(
        this.props.district.district_id
      );
      this.model['commune'].options = this.communes.map((c) => ({
        value: c.commune_id,
        label: c.name,
      }));
    }
  }

  onCommuneChange(event: any | string) {
    const value = typeof event === 'string' ? +event : +event.target.value;
    const communeObj = this.communes.find((s) => s.commune_id === value);
    if (communeObj) {
      this.props.commune = communeObj;
    }
  }

  val = '';
  onChange(event: any) {}

  imgChangeEvt(event: any) {}
  onFileClick(event: any) {
    console.log(this.uploadInput);
    this.uploadInput.nativeElement.value = '';
    this.isCopperVisible = false;
  }
  onFileChange(event: any): void {
    this.isCopperVisible = true;
    this.imgChangeEvt = event;
  }
  cropImg(e: ImageCroppedEvent) {
    this.props.cropImgPreview = e.objectUrl || '';
  }

  croppedClick() {
    this.isCopperVisible = false;

    this.validImg = true;

    this.isValidForm(true);

    this.onPropsChange.emit({ cropImgPreview: this.props.cropImgPreview });
  }

  cropperCanceled() {
    this.isCopperVisible = false;

    this.props.cropImgPreview = '';
  }
  imgLoad() {
    // display cropper tool
  }
  initCropper() {
    // init cropper
  }
  imgFailed() {
    // error msg
  }
}
