import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentifierCardComponent } from './identifier-card.component';

describe('IdentifierCardComponent', () => {
  let component: IdentifierCardComponent;
  let fixture: ComponentFixture<IdentifierCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [IdentifierCardComponent]
    });
    fixture = TestBed.createComponent(IdentifierCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
