import { Component, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { MeetingComponent } from 'src/app/modules/meeting/meeting.component';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent {
  @ViewChild(MeetingComponent) childRef: MeetingComponent;

  close() {
    this.visible = false;
    // this.childRef.ngOnDestroy();
  }

  async open() {
    this.visible = true;
  }

  visible = false;
}
