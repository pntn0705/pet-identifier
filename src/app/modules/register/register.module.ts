import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { ImageCropperModule } from 'ngx-image-cropper';
import { AxiosService } from 'src/app/providers/axios.service';
import { DynamicModule } from '../dynamic-form/dynamic-form.module';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { EmbedComponent } from './components/embed/embed.component';
import { IdentifierCardComponent } from './components/identifier-card/identifier-card.component';
import { LogoComponent } from './components/logo/logo.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { PageTitleComponent } from './components/page-title/page-title.component';
import { PaymentCardComponent } from './components/payment-card/payment-card.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { TextBoxComponent } from './components/text-box/text-box.component';
import { WarningTextComponent } from './components/warning-text/warning-text.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { HeaderComponent } from './layouts/header/header.component';
import { RegisterBodyComponent } from './layouts/register-body/register-body.component';
import { RegisterComponent } from './register.component';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { MeetingComponent } from '../meeting/meeting.component';

const components = [];

@NgModule({
  declarations: [
    HeaderComponent,
    RegisterBodyComponent,
    FooterComponent,
    LogoComponent,
    NavBarComponent,
    PageTitleComponent,
    ProgressBarComponent,
    RegisterFormComponent,
    IdentifierCardComponent,
    WarningTextComponent,
    PaymentCardComponent,
    AboutUsComponent,
    EmbedComponent,
    RegisterComponent,
    TextBoxComponent,
    TextBoxComponent,
    MeetingComponent,
  ],
  imports: [
    BrowserModule,
    NzInputModule,
    ReactiveFormsModule,
    NzIconModule,
    NzFormModule,
    NzSelectModule,
    NzDatePickerModule,
    NzDividerModule,
    FormsModule,
    ImageCropperModule,
    HttpClientModule,
    NzUploadModule,
    NzDrawerModule,
    NzModalModule,
    NzSliderModule,
    DynamicModule,
    NzMenuModule,
  ],
  providers: [AxiosService],
  exports: [HeaderComponent, FooterComponent, RegisterComponent],
})
export class RegisterModule {}
