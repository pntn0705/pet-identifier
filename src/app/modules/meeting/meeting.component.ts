import { Component, DoCheck, OnDestroy, OnInit } from '@angular/core';

import {
  ref,
  set,
  Database,
  update,
  getDatabase,
  onValue,
  DatabaseReference,
} from '@angular/fire/database';

import firebase from 'firebase/compat/app';

declare let RTCPeerConnection: any;
const config = {
  apiKey: 'AIzaSyCke_PKRYxrZARHjEI-iZNR0z3I3JoJBK0',
  databaseURL: 'https://pet-identifier-1f67f-default-rtdb.firebasedatabase.app',
  authDomain: 'pet-identifier-1f67f.firebaseapp.com',
  projectId: 'pet-identifier-1f67f',
  storageBucket: 'pet-identifier-1f67f.appspot.com',
  messagingSenderId: '7355391527',
  appId: '1:7355391527:web:390b23926c53952a4ab802',
};

export const ENV_RTCPeerConfiguration = {
  iceServers: [
    {
      urls: 'stun:stun1.l.google.com:19302',
    },
    {
      urls: 'turn:14.187.234.193:888',
      username: 'username1',
      credential: 'key1',
    },
  ],
};

interface IRoom {
  id?: string;
  code: string;
}

@Component({
  selector: 'app-meeting',
  templateUrl: 'meeting.component.html',
  styleUrls: ['meeting.component.scss'],
})
export class MeetingComponent implements OnInit, OnDestroy, DoCheck {
  pc: any;
  remoteStream: any;
  localStream: MediaStream = new MediaStream();
  recorder: MediaRecorder;
  remoteRecorder: MediaRecorder;
  roomId: string = '';
  streamStarted = false;
  senderId: string;

  offerSdp = '';
  answerSdp = '';
  inRemote = false;

  constructor(private database: Database) {}

  async init() {
    this.pc = new RTCPeerConnection(ENV_RTCPeerConfiguration);
    const localStream = await navigator.mediaDevices.getUserMedia({
      video: true,
      audio: false,
    });
    const remoteStream = new MediaStream();
    this.remoteStream = remoteStream;
    this.recorder = new MediaRecorder(localStream, {
      mimeType: 'video/webm; codecs=vp9',
    });

    this.recorder.ondataavailable = ($event) =>
      this.handleDataAvailable($event);
    this.recorder.start();

    if (localStream) {
      localStream.onaddtrack = (ev: MediaStreamTrackEvent) => {
        console.log(ev, '====');
      };
    }

    this.localStream = localStream;

    localStream.getTracks().forEach((track) => {
      console.log(track, 'tract');
      this.pc.addTrack(track, localStream);
    });

    this.pc.ontrack = (event: any) => {
      event.streams[0].getTracks().forEach((track: any) => {
        this.remoteStream.addTrack(track);
        this.remoteRecorder = new MediaRecorder(this.remoteStream, {
          mimeType: 'video/webm; codecs=vp9',
        });

        this.remoteRecorder.ondataavailable = ($event) =>
          this.handleDataAvailable($event);

        this.remoteRecorder.start();
      });
    };
  }

  async ngDoCheck(): Promise<void> {}

  async roomIdChange(event: any) {
    this.setRoomId(event.target.value);
  }

  ngOnDestroy(): void {
    this.localStream?.getTracks()?.forEach((track) => track.stop());
    this.remoteStream?.getTracks()?.forEach((track: any) => track.stop());
  }

  setRoomId(id: string) {
    if (!id) {
      return;
    }
    this.roomId = id;
    this.rtcRef = ref(this.database, 'webrtc/' + this.roomId);
    onValue(this.rtcRef, async (snapshot) => {
      const data = snapshot.val();

      if (data.answer && !this.inRemote) {
        this.answerSdp = data.answer;
        this.addAnswer();
      }
    });
  }

  createRoom() {
    this.createOffer();
  }

  test() {
    set;
  }

  async openStream() {
    if (!this.streamStarted) {
      const config = { audio: true, video: true };
      this.localStream.onaddtrack = (ev) => {
        console.log(ev);
      };
      this.localStream = await navigator.mediaDevices.getUserMedia(config);
    } else {
      this.localStream?.getVideoTracks()[0].stop();
      console.log(this.localStream?.getVideoTracks()[0].readyState);
    }
    this.streamStarted = !this.streamStarted;
  }

  ngOnInit(): void {
    this.setupRealTimeDb();
  }

  sendMessage(senderId: string, data: any) {
    // var msg = this.channel.push({ sender: senderId, message: data });
    // msg.remove();
  }

  guid() {
    this.setRoomId(this.roomId ? this.roomId : this.s4());
    return this.roomId;
  }
  s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  setupRealTimeDb() {
    // this.senderId = this.guid();
    // var channelName = '/webrtc';
    // this.channel = this.afDb.list(channelName);
    // console.log(this.channel, 1);
    // this.database = this.afDb.database.ref(channelName);
    // console.log(this.database, 2);
    // this.database.on('child_added', this.readMessage.bind(this));
  }

  rtcRef: DatabaseReference;
  recordedChunks: [] = [];
  handleDataAvailable(event: any) {
    console.log('data-available', this.recordedChunks, event.data.size);
    if (event.data.size > 0) {
      this.recordedChunks.push(event.data as never);
      console.log(this.recordedChunks);
      this.download();
    } else {
      // …
    }
  }

  download() {
    const blob = new Blob(this.recordedChunks, {
      type: 'video/webm',
    });
    const url = URL.createObjectURL(blob);

    console.log(url);
  }

  async ngAfterViewInit() {
    this.init();
  }

  createOffer = async () => {
    this.pc.onicecandidate = async (event: any) => {
      //Event that fires off when a new offer ICE candidate is created

      if (event.candidate) {
        this.offerSdp = JSON.stringify(this.pc.localDescription);
        // send this offerSdp to server

        await this.writeData({
          offer: this.offerSdp,
          localDescription: this.pc.localDescription,
        });
      }
    };

    const offer = await this.pc.createOffer();
    await this.pc.setLocalDescription(offer);
  };

  async writeData(data: any) {
    await set(ref(this.database, 'webrtc/' + this.guid()), data);
  }

  async readOffer() {
    this.inRemote = true;
    const starCountRef = ref(this.database, 'webrtc/' + this.roomId);
    onValue(starCountRef, async (snapshot) => {
      const data = snapshot.val();
      console.log('succeed', data);
      this.offerSdp = data.offer;
      await this.createAnswer();
    });
  }

  createAnswer = async () => {
    let offer = JSON.parse(this.offerSdp);

    this.pc.onicecandidate = async (event: any) => {
      //Event that fires off when a new answer ICE candidate is created
      if (event.candidate) {
        this.answerSdp = JSON.stringify(this.pc.localDescription);
        await this.writeData({ offer: this.offerSdp, answer: this.answerSdp });
      }
    };

    await this.pc.setRemoteDescription(offer);

    let answer = await this.pc.createAnswer();
    await this.pc.setLocalDescription(answer);
  };

  addAnswer = async () => {
    let answer = JSON.parse(this.answerSdp);

    if (!this.pc.currentRemoteDescription) {
      this.pc.setRemoteDescription(answer);
    }
  };
}
