import {
  AfterViewInit,
  Component,
  OnChanges,
  OnDestroy,
  SimpleChanges,
} from '@angular/core';

import * as ReactDOM from 'react-dom';
import * as React from 'react';
import App from '.';

@Component({
  template: `<div [id]="rootId"></div>`,
  selector: 'app-chat',
})
export class ChatComponent implements OnChanges, OnDestroy, AfterViewInit {
  ngOnChanges(changes: SimpleChanges): void {
    this.render();
  }
  ngOnDestroy(): void {}
  ngAfterViewInit(): void {
    this.render();
  }
  rootId = 'rootId';

  render() {
    ReactDOM.render(
      React.createElement(App),
      document.getElementById(this.rootId)
    );
  }
}
