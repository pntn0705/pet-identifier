import {
  CloseCircleFilled,
  CommentOutlined,
  SendOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Button, Card, Flex, FloatButton, Input } from "antd";
import * as React from "react";
import { ChangeEvent, useEffect, useRef, useState } from "react";
import Message from "./components/message";
import { isNil } from "lodash";

interface IMessage {
  username: string;
  message: string;
  date: Date;
  avatar?: string;
  ownMessage?: boolean;
}

function App() {
  const [message, setMessage] = useState("");
  const onChange = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setMessage(event.target.value);
  };

  let messagesEndRef = useRef<HTMLDivElement>(null);

  const onKeyDown = (
    event: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    if (event.key === "Enter") {
      submit();
    }
  };

  const submit = () => {
    const tmpMessage = message;
    setMessage("");
    const newMessages = [...messages];
    newMessages.push({
      username,
      message: tmpMessage,
      date: new Date(),
      ownMessage: true,
    });
    setMessages(newMessages);
  };

  const [messages, setMessages] = useState<IMessage[]>([]);
  useEffect(() => {
    if (messages.length === 0) {
      setMessages([
        {
          username: "admin",
          message: "hello there",
          date: new Date(),
        },
      ]);
    }
  }, [messages]);

  useEffect(() => {
    const maxScrollTop =
      (messagesEndRef.current?.scrollHeight || 0) -
      (messagesEndRef.current?.clientHeight || 0);
    // messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
    console.log(maxScrollTop, messagesEndRef.current?.scrollTop);
    if (messagesEndRef.current && !isNil(messagesEndRef.current.scrollTop)) {
      messagesEndRef.current.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
    }
  }, [messages]);

  const [messageOn, setMessageOn] = useState(false);
  const [username, setUsername] = useState("");
  const [isUsernameWasSet, setIsUsernameWasSet] = useState(false);

  const onUsernameChange = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setUsername(e.target.value);
  };

  const unUsernameSubmit = (
    e: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    console.log(e.key);
    if (e.key === "Enter") {
      setIsUsernameWasSet(true);
      return;
    }
  };

  return messageOn ? (
    !isUsernameWasSet ? (
      <Card
        title={
          <Flex justify="space-between" align="center">
            <div>Message</div>
            <CloseCircleFilled
              onClick={() => {
                setMessageOn(!messageOn);
                setMessages([]);
              }}
              className="close-button"
            />
          </Flex>
        }
        className="username-input"
      >
        <Input
          size="large"
          placeholder="Hãy nhập tên của bạn"
          prefix={<UserOutlined />}
          value={username}
          onKeyDown={unUsernameSubmit}
          onChange={onUsernameChange}
        />
      </Card>
    ) : (
      <div className="chat-box">
        <Card
          title={
            <Flex justify="space-between" align="center">
              <div>Message</div>
              <CloseCircleFilled
                onClick={() => {
                  setMessageOn(!messageOn);
                  setMessages([]);
                }}
                className="close-button"
              />
            </Flex>
          }
          className="card"
        >
          <Flex vertical={true}>
            <div className="chat-zone" ref={messagesEndRef}>
              {messages.map((message) => (
                <Message
                  username={message.username}
                  avatar={message.avatar}
                  date={message.date}
                  message={message.message}
                  ownMessage={message.ownMessage}
                ></Message>
              ))}
            </div>
            <Flex
              className="message-control"
              align="center"
              justify="space-between"
            >
              <Input
                className="message-input"
                showCount
                maxLength={200}
                value={message}
                onChange={onChange}
                onKeyDown={onKeyDown}
              />
              <Button
                className="message-submit"
                onClick={() => submit()}
                disabled={message.length === 0}
                type="primary"
                icon={<SendOutlined />}
              />
            </Flex>
          </Flex>
        </Card>
      </div>
    )
  ) : (
    <FloatButton
      onClick={() => setMessageOn(!messageOn)}
      icon={<CommentOutlined />}
    />
  );
}

export default App;
