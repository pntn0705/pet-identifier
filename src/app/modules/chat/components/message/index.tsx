import * as React from "react";
import { Flex } from "antd";

function Message(props: {
  message: string;
  username: string;
  date: Date;
  avatar?: string;
  ownMessage?: boolean;
}) {
  return !props ? (
    <></>
  ) : (
    <div className="message">
      <Flex
        justify={props.ownMessage ? "flex-end" : "flex-start"}
        align="center"
      >
        <div className="message-user-avatar">
          <img
            src={
              props.avatar ||
              "https://yt3.ggpht.com/7QgQySkuThMtiuVii_aAB4tW5y9phjcp7ZFSxeCvvfcjHxfTfhGRMLnNWYASoMzx3iNya01J_A=s48-c-k-c0x00ffffff-no-rj"
            }
            alt=""
          />
        </div>
        <div className="message-username">{props.username}</div>
      </Flex>
      <div
        className={
          props.ownMessage ? "message-content own-message" : "message-content"
        }
      >
        <span>{props.message}</span>
      </div>
      <div
        className="message-time"
        style={{ textAlign: props.ownMessage ? "right" : "left" }}
      >
        {new Date(props.date).toISOString()}
      </div>
    </div>
  );
}

export default Message;
