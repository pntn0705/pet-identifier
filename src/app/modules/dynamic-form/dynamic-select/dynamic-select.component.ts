import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IDynamicFormField } from '../interfaces/dynamic-form-field.interface';

@Component({
  selector: 'app-dynamic-select',
  templateUrl: './dynamic-select.component.html',
  styleUrls: ['./dynamic-select.component.scss'],
})
export class DynamicSelectComponent {
  @Input() field: IDynamicFormField;
  @Input() formName: FormGroup;
  @Input({ required: true })
  background: string;
  @Output()
  change = new EventEmitter();

  onChangeEvent(value: any) {
    this.change.next(value);
  }
}
