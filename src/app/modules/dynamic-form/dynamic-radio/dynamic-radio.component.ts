import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IDynamicFormField } from '../interfaces/dynamic-form-field.interface';

@Component({
  selector: 'app-dynamic-radio',
  templateUrl: './dynamic-radio.component.html',
  styleUrls: ['./dynamic-radio.component.scss'],
})
export class DynamicRadioComponent {
  @Input() field: IDynamicFormField;
  @Input() formName: FormGroup;
  @Input({ required: true }) background: string;
}
