import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { IDynamicFormModel } from './interfaces/dynamic-form.model';
import {
  DynamicModel,
  IDynamicFormField,
} from './interfaces/dynamic-form-field.interface';
import { compact } from 'lodash';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss'],
})
export class DynamicFormComponent implements OnInit, IDynamicFormModel {
  fields: IDynamicFormField[] = [];

  //#region input output
  @Input({ required: true })
  model: DynamicModel;
  @Input()
  title: string = '';
  @Input()
  isHorizontal = false;
  @Input()
  col = 2;
  @Input()
  background: string = 'white';

  @Output()
  formGroupEvent = new EventEmitter<any>();
  @Output()
  isValid = new EventEmitter<boolean>();
  //#endregion input

  formValidate(): boolean {
    for (const field of this.fields) {
      const formControl = this._formGroup.get(field.fieldName || '');
      if (formControl && formControl.invalid) {
        console.log(field, 'invalid');
        return false;
      }
      console.log(field, 'valid');
    }
    return true;
  }

  _formGroup: FormGroup;

  getFormControlFields(): Record<string, FormControl> {
    const formGroupFields: Record<string, FormControl> = {};
    for (const field of Object.keys(this.model)) {
      this.model[field].fieldName = field;
      this.model[field].label = this.model[field].label || field;

      formGroupFields[field] = new FormControl(
        this.model[field].defaultValue,
        this.model[field].validators || []
      );
      this.model[field].errorMessage =
        this.model[field].errorMessage ||
        `${this.model[field].label} is not valid`;
      formGroupFields[field].valueChanges.subscribe((val) => {
        this.model[field].defaultValue = val;
        this.formGroupEvent.emit({
          name: field,
          value: val,
          type: this.model[field].type,
        });
        this.isValid.next(this.formValidate());
      });
      this.fields.push(this.model[field]);
    }
    return formGroupFields;
  }

  buildForm(): void {
    const formGroupFields = this.getFormControlFields();
    this._formGroup = new FormGroup(formGroupFields);
  }

  ngOnInit() {
    this.buildForm();
  }
}
