import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IDynamicFormField } from '../interfaces/dynamic-form-field.interface';

@Component({
  selector: 'app-dynamic-checkbox',
  templateUrl: './dynamic-checkbox.component.html',
  styleUrls: ['./dynamic-checkbox.component.scss'],
})
export class DynamicCheckboxComponent {
  @Input() field: IDynamicFormField;
  @Input() formName: FormGroup;
  @Input({ required: true }) background: string;
}
