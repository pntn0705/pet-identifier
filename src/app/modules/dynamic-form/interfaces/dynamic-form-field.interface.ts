import { FormControl, ValidatorFn } from '@angular/forms';

export interface IDynamicFormField {
  defaultValue?: any;
  type?: string;
  label?: string;
  fieldName?: string;
  options?: {
    label: any;
    value: any;
  }[];
  function?: {
    change(value: any): void;
  };
  validators?: ValidatorFn[];
  errorMessage?: string;
}

export type DynamicModel = Record<string, IDynamicFormField> & {
  isValid?: boolean;
};
