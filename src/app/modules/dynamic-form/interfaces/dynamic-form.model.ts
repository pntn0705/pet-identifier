import { FormControl } from '@angular/forms';
import {
  DynamicModel,
  IDynamicFormField,
} from './dynamic-form-field.interface';

export interface IDynamicFormModel {
  getFormControlFields(): Record<string, FormControl>;
  buildForm(): void;
  model: DynamicModel;
  fields?: IDynamicFormField[];
}
