import { Component, Input } from '@angular/core';
import { IDynamicFormField } from '../interfaces/dynamic-form-field.interface';

@Component({
  selector: 'app-dynamic-input',
  templateUrl: './dynamic-input.component.html',
  styleUrls: ['./dynamic-input.component.scss'],
})
export class DynamicInputComponent {
  @Input({ required: true }) field: IDynamicFormField;
  @Input({ required: true }) formName: any;
  @Input({ required: true }) background: string;
}
